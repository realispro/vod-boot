package ms.cinemas.web;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ms.cinemas.dao.CinemaRepository;
import ms.cinemas.model.Cinema;
import ms.cinemas.model.Movie;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Slf4j
@RestController
@RequestMapping("/webapi")
@RequiredArgsConstructor
public class CinemaRest {

    private final CinemaRepository cinemaRepository;

    @Value("${vod.ms.movie.location}")
    private String movieServiceLocation;

    @GetMapping("/cinemas")
    public List<Cinema> getCinemas() {
        log.info("about to retrieve all cinemas");
        List<Cinema> cinemas = cinemaRepository.findAll();
        cinemas.forEach(c->fillMovies(c));
        return cinemas;
    }

    @GetMapping("/cinemas/{id}")
    public ResponseEntity<Cinema> getCinema(@PathVariable("id") int id){
        log.info("retrieving movie {}", id);
        Optional<Cinema> cinema = cinemaRepository.findById(id);
        if(cinema.isPresent()){
            Cinema c = cinema.get();
            fillMovies(c);
            return ResponseEntity.ok(c);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/movies/{id}/cinemas")
    public List<Cinema> getCinemasByMovie(@PathVariable int id) {
        log.info("about to retrieve cinemas by movie {}", id);
        List<Cinema> cinemas = cinemaRepository.findCinemasByMoviesIsContaining(id);
        cinemas.forEach(c->fillMovies(c));
        return cinemas;
    }

    private void fillMovies(Cinema cinema){
        List<Movie> movies = new ArrayList<>();

        cinema.getMovies().forEach(
                id->movies.add(getMovie(id))
        );

        cinema.setMoviesData(movies);
    }

    private Movie getMovie(int movieId){
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Movie> responseEntity = restTemplate.exchange(
                movieServiceLocation + movieId,
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    Movie.class
                );
        Movie movie = responseEntity.getBody();
        log.info("response entity: code={} payload={}", responseEntity.getStatusCode(), movie);

        return movie;
    }

}
