package ms.cinemas.model;

import lombok.Data;

@Data
public class Movie {

    private int id;
    private String title;
    private float rating;

}
