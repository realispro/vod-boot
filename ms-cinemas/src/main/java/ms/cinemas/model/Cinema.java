package ms.cinemas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Data
@Entity
public class Cinema implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String logo;

    @JsonIgnore
    @ElementCollection
    @CollectionTable(name = "MOVIE_CINEMA", joinColumns = @JoinColumn(name = "cinema_id"))
    @Column(name = "movie_id")
    public Set<Integer> movies;

    @Transient
    public List<Movie> moviesData;

    @Override
    public String toString() {
        return "Cinema{" +
                "name='" + name + '\'' +
                ", logo='" + logo + '\'' +
                '}';
    }
}
