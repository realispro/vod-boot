package vod.boot;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import vod.boot.service.CinemaService;

//@Component
@RequiredArgsConstructor
@Slf4j
public class VodCommandLine implements CommandLineRunner {

    private final CinemaService cinemaService;

    @Override
    public void run(String... args) throws Exception {
        log.info("starting VOD...");
        cinemaService.getAllCinemas()
                .forEach(c->log.info("cinema: {}", c));
    }
}
