package vod.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootVodApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootVodApplication.class, args);
    }

}
