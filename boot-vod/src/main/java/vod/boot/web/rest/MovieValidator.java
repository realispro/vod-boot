package vod.boot.web.rest;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import vod.boot.model.Movie;

@Component
public class MovieValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Movie.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Movie movie = (Movie) target;

        if(movie.getTitle()==null || movie.getTitle().trim().isEmpty()){
            errors.rejectValue("title", "error.title.empty");
        }
        if(movie.getRating()<0 || movie.getRating()>5){
            errors.rejectValue("rating", "error.rating.out.of.range");
        }

    }
}
