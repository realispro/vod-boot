package vod.boot.web.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vod.boot.model.Cinema;
import vod.boot.model.Movie;
import vod.boot.service.CinemaService;
import vod.boot.service.MovieService;

import java.util.Set;

@RestController
@RequestMapping("/webapi")
@Slf4j
@RequiredArgsConstructor
public class CinemaREST {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @GetMapping("/cinemas")
    Set<Cinema> getCinemas() {
        log.info("retrieving cinemas list");
        return cinemaService.getAllCinemas();
    }
    @GetMapping("/cinemas/{id}")
    Cinema getCinema(@PathVariable("id") int id){
        log.info("retrieving cinema {}", id);

        if(id==99){
            throw new IllegalArgumentException("99 id not allowed");
        }

        return  cinemaService.getCinemaById(id);
    }

    @GetMapping("/cinemas/{id}/movies")
    ResponseEntity<Set<Movie>> getMoviesInCinema(@PathVariable("id") int cinemaId){
        log.info("retrieving movies played in a cinema {}", cinemaId);
        Cinema cinema = cinemaService.getCinemaById(cinemaId);


        if(cinema!=null){
            return ResponseEntity.ok(cinemaService.getMoviesInCinema(cinema));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/movies/{id}/cinemas")
    ResponseEntity<Set<Cinema>> getCinemasByMovie(@PathVariable("id")int movieId){
        log.info("retrieving cinemas playing movie {}", movieId);
        Movie movie = movieService.getMovieById(movieId);
        if (movie != null)
            return ResponseEntity.ok(cinemaService.getCinemasByMovie(movie));
        else
            return ResponseEntity.notFound().build();
    }
}