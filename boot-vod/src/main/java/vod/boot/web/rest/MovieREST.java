package vod.boot.web.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vod.boot.model.Movie;
import vod.boot.service.MovieService;

import java.util.Locale;
import java.util.Set;

@RestController
@RequestMapping("/webapi")
@RequiredArgsConstructor
@Slf4j
public class MovieREST {

    private final MovieService movieService;
    private final MessageSource messageSource;


    @GetMapping("/movies")
    Set<Movie> getMovies(){
        log.info("retrieving movie list");
        return movieService.getAllMovies();
    }

    @GetMapping("/movies/{id}")
    ResponseEntity<Movie> getMovie(@PathVariable("id") int id){
        log.info("retrieving movie " + id);
        Movie movie = movieService.getMovieById(id);
        if(movie==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.status(200).body(movie);
                   //new ResponseEntity<>(movie, HttpStatus.OK);
        }
    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(@Validated @RequestBody Movie movie, Errors errors, Locale locale){
        log.info("adding movie " + movie);

        //Locale locale = Locale.getDefault();
                //new Locale("pl", "PL");
        // messages_pl_PL
        // messages_pl
        // messages_<OS>
        // messages

        boolean valid = !errors.hasErrors();

        if(!valid){
            StringBuilder sb = new StringBuilder();
            errors.getAllErrors().forEach(
                  e -> sb.append(
                       messageSource.getMessage(e.getCode(), e.getArguments(), locale)
                  ).append("\n")
            );
            return new ResponseEntity<>(sb.toString(), HttpStatus.BAD_REQUEST);
        }

        if(movie.getTitle().equals("Foo")){
            throw new IllegalArgumentException("Foo not allowed");
        }

        movie = movieService.addMovie(movie);
        return new ResponseEntity<>(movie, HttpStatus.CREATED);

    }




}
