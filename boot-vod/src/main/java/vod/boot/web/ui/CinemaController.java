package vod.boot.web.ui;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import vod.boot.model.Cinema;
import vod.boot.model.Movie;
import vod.boot.service.CinemaService;
import vod.boot.service.MovieService;

import java.util.Set;

@RequiredArgsConstructor
@Slf4j
@Controller
@RequestMapping("/cinemas")
public class CinemaController {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @GetMapping
    String getCinemas(Model model,
                     @RequestParam(value = "movieId", required = false) Integer movieId) {
        if (movieId != null) {
            log.info("Retrieving cinemas by movie id: {}, to ui model", movieId);
        } else {
            log.info("Retrieving all cinemas.");
        }
        Set<Cinema> cinemaSet;
        if (movieId != null) {
            Movie movie = movieService.getMovieById(movieId);
            cinemaSet = cinemaService.getCinemasByMovie(movie);
            model.addAttribute("title", "Cinemas with movie: " + movie.getTitle());
        } else {
            cinemaSet = cinemaService.getAllCinemas();
            model.addAttribute("title", "All cinemas");
        }
        model.addAttribute("cinemas", cinemaSet);

        return "/cinemasView";
    }
}
