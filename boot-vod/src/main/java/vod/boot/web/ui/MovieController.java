package vod.boot.web.ui;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import vod.boot.model.Cinema;
import vod.boot.model.Director;
import vod.boot.model.Movie;
import vod.boot.service.CinemaService;
import vod.boot.service.MovieService;

import java.util.Map;
import java.util.Set;

@Controller
@RequiredArgsConstructor
@Slf4j
public class MovieController {

    private final MovieService movieService;
    private final CinemaService cinemaService;

    @GetMapping("/movies") // /movies?directorId=99
    String getMovies(
            Model model,
            @RequestParam(value = "directorId", required = false) Integer directorId,
            @RequestParam(value = "cinemaId", required = false) Integer cinemaId,
            @RequestHeader(value = "User-Agent", required = false) String userAgent,
            @RequestHeader Map<String, String> headers,
            @CookieValue(value = "JSESSIONID", required = false) String sessionId
    ){
        log.info("retrieving movies by director {}", directorId);
        log.info("session id: {}", sessionId);
        log.info("user=agent: {}", userAgent);
        headers.entrySet()
                .stream()
                .filter(e->e.getKey().startsWith("Sec-"))
                .forEach(e->log.info("header: {}:{}", e.getKey(), e.getValue()));

        Set<Movie> movies;
        if(directorId!=null) {
            Director director = movieService.getDirectorById(directorId);
            movies = movieService.getMoviesByDirector(director);
            model.addAttribute("title", "Movies by " + director.getLastName());
        } else if(cinemaId != null){
            Cinema cinema = cinemaService.getCinemaById(cinemaId);
            movies = cinemaService.getMoviesInCinema(cinema);
            model.addAttribute("title", "Movies by cinema: " + cinema.getName());
        } else {
            movies = movieService.getAllMovies();
            model.addAttribute("title", "All movies");
        }
        model.addAttribute("movies", movies);

        return "moviesView";

    }

}
