package vod.boot.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(basePackages = "vod.boot.web.ui")
@Slf4j
public class VodUiAdvice {


    @ExceptionHandler(IllegalArgumentException.class)
    String handleException(IllegalArgumentException e, Model model){
        log.error("error in UI layer", e);
        model.addAttribute("errorMessage", e.getMessage());
        model.addAttribute("title", "Error: " + e.getClass().getName());
        return "errorView";
    }
}
