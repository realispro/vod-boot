package vod.boot.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import vod.boot.web.rest.MovieValidator;

@ControllerAdvice(basePackages = "vod.boot.web.rest")
@RequiredArgsConstructor
@Slf4j
public class VodAdvice {

    private final MovieValidator validator;

    @InitBinder
    void initBinder(WebDataBinder binder){
        binder.setValidator(validator);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity<?> handleException(IllegalArgumentException e){
        log.error("unhandled exception received:", e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.I_AM_A_TEAPOT);
    }
}
