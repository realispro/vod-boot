package vod.boot.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"cinemas", "director"})
@ToString(exclude = {"cinemas", "director"})
@Entity
@Table(name = "movie")
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    private String poster;
    @ManyToOne
    @JoinColumn(name = "director_id")
    private Director director; // <fieldName>_id
    private float rating;
    @ManyToMany
    @JoinTable(
            name = "movie_cinema",
            joinColumns = @JoinColumn(name = "movie_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name="cinema_id", referencedColumnName = "id")
    )
    private Set<Cinema> cinemas = new HashSet<>();

    public Movie(int id, String title, String poster, Director director, float rating) {
        this.id = id;
        this.title = title;
        this.poster = poster;
        this.director = director;
        this.rating = rating;
    }

    public void addCinema(Cinema c) {
        this.cinemas.add(c);
    }

}
