package vod.boot.service;

import vod.boot.model.Cinema;
import vod.boot.model.Movie;

import java.util.Set;

public interface CinemaService {

    Cinema getCinemaById(int id);

    Set<Cinema> getAllCinemas();

    Set<Cinema> getCinemasByMovie(Movie m);

    Set<Movie> getMoviesInCinema(Cinema c);

}
