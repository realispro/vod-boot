package vod.boot.repository.data;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import vod.boot.model.Cinema;
import vod.boot.model.Movie;
import vod.boot.repository.CinemaDao;

import java.util.HashSet;
import java.util.Set;

@Repository
@Primary
@RequiredArgsConstructor
public class DataCinemaDao implements CinemaDao {

    private final CinemaRepository repository;
    @Override
    public Set<Cinema> findAll() {
        return new HashSet<>(
                repository.findAll()
        );
    }

    @Override
    public Cinema findById(int id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Set<Cinema> findByMovie(Movie m) {
        return new HashSet<>(
                repository.findAllByMovie(m)
        );
    }
}
