package vod.boot.repository.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vod.boot.model.Cinema;
import vod.boot.model.Director;
import vod.boot.model.Movie;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Integer> {

    List<Movie> findAllByTitleAndRatingGreaterThan(String title, float rating);

    List<Movie> findAllByDirector(Director director);

    @Query("select m from Movie m inner join m.cinemas cinema where cinema=:cinema")
    List<Movie> findAllByCinema(@Param("cinema") Cinema cinema);
}
