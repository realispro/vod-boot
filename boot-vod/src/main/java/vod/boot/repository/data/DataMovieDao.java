package vod.boot.repository.data;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import vod.boot.model.Cinema;
import vod.boot.model.Director;
import vod.boot.model.Movie;
import vod.boot.repository.MovieDao;

import java.util.HashSet;
import java.util.Set;

@Repository
@Primary
@RequiredArgsConstructor
public class DataMovieDao implements MovieDao {


    private final MovieRepository repository;

    @Override
    public Set<Movie> findAll() {
        return new HashSet<>(
                repository.findAll()
        );
    }

    @Override
    public Movie findById(int id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Set<Movie> findByDirector(Director d) {
        return new HashSet<>(
                repository.findAllByDirector(d)
        );
    }

    @Override
    public Set<Movie> findByCinema(Cinema c) {
        return new HashSet<>(
                repository.findAllByCinema(c)
        );
    }

    @Override
    public Movie add(Movie m) {
        return repository.save(m);
    }
}
