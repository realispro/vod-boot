package vod.boot.repository.data;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import vod.boot.model.Director;
import vod.boot.repository.DirectorDao;

import java.util.HashSet;
import java.util.Set;

@Repository
@Primary
@RequiredArgsConstructor
public class DataDirectorDao implements DirectorDao {

    private final DirectorRepository repository;
    @Override
    public Set<Director> findAll() {
        return new HashSet<>(
                repository.findAll()
        );
    }

    @Override
    public Director findById(int id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Director add(Director d) {
        return repository.save(d);
    }
}
