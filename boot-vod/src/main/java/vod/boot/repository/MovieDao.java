package vod.boot.repository;

import vod.boot.model.Cinema;
import vod.boot.model.Director;
import vod.boot.model.Movie;

import java.util.Set;

public interface MovieDao {

    Set<Movie> findAll();

    Movie findById(int id);

    Set<Movie> findByDirector(Director d);

    Set<Movie> findByCinema(Cinema c);

    Movie add(Movie m);

}
