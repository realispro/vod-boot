package vod.boot.repository.mem;

import org.springframework.stereotype.Component;
import vod.boot.model.Cinema;
import vod.boot.model.Director;
import vod.boot.model.Movie;
import vod.boot.repository.MovieDao;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class MemMovieDao implements MovieDao {
    @Override
    public Set<Movie> findAll() {
        return SampleData.movies;
    }

    @Override
    public Movie findById(int id) {
        return SampleData.movies.stream().filter(m -> m.getId() == id).findFirst().orElse(null);
    }

    @Override
    public Set<Movie> findByDirector(Director d) {
        return SampleData.movies.stream().filter(m -> m.getDirector() == d).collect(Collectors.toSet());
    }

    @Override
    public Set<Movie> findByCinema(Cinema c) {
        return SampleData.movies.stream().filter(m -> m.getCinemas().contains(c)).collect(Collectors.toSet());
    }

    @Override
    public Movie add(Movie m) {
        int max = SampleData.movies.stream().max((m1, m2) -> m1.getId() - m2.getId()).get().getId();
        m.setId(++max);
        SampleData.movies.add(m);
        return m;
    }
}
