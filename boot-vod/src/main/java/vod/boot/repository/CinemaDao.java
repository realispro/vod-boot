package vod.boot.repository;

import vod.boot.model.Cinema;
import vod.boot.model.Movie;

import java.util.Set;

public interface CinemaDao {

    Set<Cinema> findAll();

    Cinema findById(int id);

    Set<Cinema> findByMovie(Movie m);

}
