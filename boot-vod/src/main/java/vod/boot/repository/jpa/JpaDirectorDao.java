package vod.boot.repository.jpa;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import vod.boot.model.Director;
import vod.boot.repository.DirectorDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.Set;

@Repository
//@Primary
public class JpaDirectorDao implements DirectorDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Set<Director> findAll() {
        return new HashSet<>(entityManager.createQuery("select d from Director d").getResultList());
    }

    @Override
    public Director findById(int id) {
        return entityManager.find(Director.class, id);
    }

    @Override
    public Director add(Director d) {
        entityManager.persist(d);
        return d;
    }
}
