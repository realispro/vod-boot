package vod.boot.repository.jpa;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import vod.boot.model.Cinema;
import vod.boot.model.Movie;
import vod.boot.repository.CinemaDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.Set;

@Repository
//@Primary
public class JpaCinemaDao implements CinemaDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Set<Cinema> findAll() {
        // JPQL -> HQL -> SQL
        return new HashSet<>(entityManager.createQuery("select c from Cinema c").getResultList());
    }

    @Override
    public Cinema findById(int id) {
        return entityManager.find(Cinema.class, id);
    }

    @Override
    public Set<Cinema> findByMovie(Movie m) {
        return new HashSet<>(entityManager
                .createQuery("select c from Cinema c inner join c.movies movie where movie=:movie")
                .setParameter("movie", m)
                .getResultList());
    }
}
