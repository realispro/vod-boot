package vod.boot.repository.jpa;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vod.boot.model.Cinema;
import vod.boot.model.Director;
import vod.boot.model.Movie;
import vod.boot.repository.MovieDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.Set;

@Repository
public class JpaMovieDao implements MovieDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Set<Movie> findAll() {
        return new HashSet<>(entityManager.createQuery("select m from Movie m").getResultList());
    }

    @Override
    public Movie findById(int id) {
        return entityManager.find(Movie.class, id);
    }

    @Override
    public Set<Movie> findByDirector(Director d) {
        return new HashSet<>(
                entityManager
                        .createQuery("select m from Movie m where m.director = :director")
                        .setParameter("director", d)
                        .getResultList());
    }

    @Override
    public Set<Movie> findByCinema(Cinema c) {
        return new HashSet<>(
                entityManager
                        .createQuery("select m from Movie m inner join m.cinemas cinema where cinema=:cinema")
                        .setParameter("cinema", c)
                        .getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public Movie add(Movie m) {
        entityManager.persist(m);
        return m;
    }
}
