package vod.web.ui;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import vod.model.Movie;
import vod.service.MovieService;

import java.util.Set;

@Controller
public class MovieController {

    private final MovieService movieService;

    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/movies")
    String getMovies(Model model){

        Set<Movie> movies = movieService.getAllMovies();

        model.addAttribute("movies", movies);

        return "movies";

    }

}
