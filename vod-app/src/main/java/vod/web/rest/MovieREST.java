package vod.web.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import vod.model.Movie;
import vod.service.MovieService;

import java.util.Locale;
import java.util.Set;
import java.util.logging.Logger;

@RestController
@RequestMapping("/webapi")
@RequiredArgsConstructor
public class MovieREST {

    private static final Logger log = Logger.getLogger(MovieREST.class.getName());

    private final MovieService movieService;
    private final MovieValidator validator;
    private final MessageSource messageSource;



    @InitBinder
    void initBinder(WebDataBinder binder){
        binder.setValidator(validator);
    }

    @GetMapping("/movies")
    Set<Movie> getMovies(){
        log.info("retrieving movie list");
        return movieService.getAllMovies();
    }

    @GetMapping("/movies/{id}")
    ResponseEntity<Movie> getMovie(@PathVariable("id") int id){
        log.info("retrieving movie " + id);
        Movie movie = movieService.getMovieById(id);
        if(movie==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.status(200).body(movie);
                   //new ResponseEntity<>(movie, HttpStatus.OK);
        }
    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(@Validated @RequestBody Movie movie, Errors errors, Locale locale){
        log.info("adding movie " + movie);

        //Locale locale = Locale.getDefault();
                //new Locale("pl", "PL");
        // messages_pl_PL
        // messages_pl
        // messages_<OS>
        // messages

        boolean valid = !errors.hasErrors();

        if(!valid){
            StringBuilder sb = new StringBuilder();
            errors.getAllErrors().forEach(
                  e -> sb.append(
                       messageSource.getMessage(e.getCode(), e.getArguments(), locale)
                  ).append("\n")
            );
            return new ResponseEntity<>(sb.toString(), HttpStatus.BAD_REQUEST);
        }

        movie = movieService.addMovie(movie);
        return new ResponseEntity<>(movie, HttpStatus.CREATED);

    }



}
