package vod.web.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vod.model.Cinema;
import vod.service.CinemaService;

import java.util.Set;
import java.util.logging.Logger;

@RestController
@RequestMapping("/webapi")
public class CinemaREST {
    private static final Logger log = Logger.getLogger(CinemaREST.class.getName());
    private final CinemaService cinemaService;
    public CinemaREST(CinemaService cinemaService) {
        this.cinemaService = cinemaService;
    }
    @GetMapping("/cinemas")
    Set<Cinema> getCinemas() {
        log.info("retrieving cinemas list");
        return cinemaService.getAllCinemas();
    }
    @GetMapping("/cinemas/{id}")
    Cinema getCinema(@PathVariable("id") int id){
        log.info("retrieving cinema " + id);
        return  cinemaService.getCinemaById(id);
    }
}