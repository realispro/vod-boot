package vod.repository;

import vod.model.Cinema;
import vod.model.Movie;

import java.util.Set;

public interface CinemaDao {

    Set<Cinema> findAll();

    Cinema findById(int id);

    Set<Cinema> findByMovie(Movie m);

}
