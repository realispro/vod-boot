package vod.repository.mem;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import vod.repository.CinemaDao;
import vod.model.Cinema;
import vod.model.Movie;

import java.util.Set;
import java.util.stream.Collectors;


@Component
@Primary
public class MemCinemaDao implements CinemaDao {

    @Override
    public Set<Cinema> findAll() {
        return SampleData.cinemas;
    }

    @Override
    public Cinema findById(int id) {
        return SampleData.cinemas.stream().filter(c -> c.getId() == id).findFirst().orElse(null);
    }

    @Override
    public Set<Cinema> findByMovie(Movie m) {
        return SampleData.cinemas.stream().filter(c -> c.getMovies().contains(m)).collect(Collectors.toSet());
    }
}
