package vod.repository.tmp;

import org.springframework.stereotype.Component;
import vod.model.Cinema;
import vod.model.Movie;
import vod.repository.CinemaDao;

import java.util.Set;

@Component("cinemaDao")
public class TmpCinemaDao implements CinemaDao {
    @Override
    public Set<Cinema> findAll() {
        return null;
    }

    @Override
    public Cinema findById(int id) {
        return null;
    }

    @Override
    public Set<Cinema> findByMovie(Movie m) {
        return null;
    }
}
