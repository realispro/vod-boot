package vod.repository;

import vod.model.Director;

import java.util.Set;

public interface DirectorDao {

    Set<Director> findAll();

    Director findById(int id);

    Director add(Director d);


}
