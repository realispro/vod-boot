package vod.repository;

import vod.model.Cinema;
import vod.model.Director;
import vod.model.Movie;

import java.util.Set;

public interface MovieDao {

    Set<Movie> findAll();

    Movie findById(int id);

    Set<Movie> findByDirector(Director d);

    Set<Movie> findByCinema(Cinema c);

    Movie add(Movie m);

}
