package vod.service;

import vod.model.Director;
import vod.model.Movie;

import java.util.Set;

public interface MovieService {


    Set<Movie> getAllMovies();

    Set<Movie> getMoviesByDirector(Director d);

    Movie getMovieById(int id);

    Movie addMovie(Movie m);


    Set<Director> getAllDirectors();

    Director getDirectorById(int id);

    Director addDirector(Director d);
}
