package vod.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import vod.repository.CinemaDao;
import vod.repository.MovieDao;
import vod.repository.mem.MemCinemaDao;
import vod.repository.mem.MemMovieDao;
import vod.model.Cinema;
import vod.service.impl.CinemaServiceBean;

import java.util.Set;

public class VodServiceMain {

    public static void main(String[] args) {
        System.out.println("Let's find cinemas!");

        // service preparation
        ApplicationContext context = new AnnotationConfigApplicationContext("vod");
                //new ClassPathXmlApplicationContext("classpath:context.xml");

        CinemaService service = context.getBean(CinemaService.class);
        CinemaService service2 = context.getBean(CinemaService.class);


        // service use
        Set<Cinema> cinemas = service.getAllCinemas();
        System.out.println(cinemas.size() + " cinemas found:");
        cinemas.forEach(System.out::println);
    }
}
