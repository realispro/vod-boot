package vod.service.impl;

import org.springframework.stereotype.Component;
import vod.repository.CinemaDao;
import vod.repository.DirectorDao;
import vod.repository.MovieDao;
import vod.model.Cinema;
import vod.model.Director;
import vod.model.Movie;
import vod.service.MovieService;

import java.util.Set;
import java.util.logging.Logger;

@Component
public class MovieServiceBean implements MovieService {

    private static final Logger log = Logger.getLogger(MovieService.class.getName());

    private DirectorDao directorDao;
    private CinemaDao cinemaDao;
    private MovieDao movieDao;

    public MovieServiceBean(DirectorDao directorDao, CinemaDao cinemaDao, MovieDao movieDao) {
        this.directorDao = directorDao;
        this.cinemaDao = cinemaDao;
        this.movieDao = movieDao;
    }

    public Set<Movie> getAllMovies() {
        log.info("searching all movies...");
        return movieDao.findAll();
    }

    public Set<Movie> getMoviesByDirector(Director d) {
        log.info("serching movies by diretor " + d.getId());
        return movieDao.findByDirector(d);
    }

    public Set<Movie> getMoviesInCinema(Cinema c) {
        log.info("searching movies played in cinema " + c.getId());
        return movieDao.findByCinema(c);
    }

    public Movie getMovieById(int id) {
        log.info("searching movie by id " + id);
        return movieDao.findById(id);
    }

    public Set<Cinema> getAllCinemas() {
        log.info("searching all cinemas");
        return cinemaDao.findAll();
    }

    public Set<Cinema> getCinemasByMovie(Movie m) {
        log.info("searching cinemas by movie " + m.getId());
        return cinemaDao.findByMovie(m);
    }

    public Cinema getCinemaById(int id) {
        log.info("searching cinema by id " + id);
        return cinemaDao.findById(id);
    }

    public Set<Director> getAllDirectors() {
        log.info("searching all directors");
        return directorDao.findAll();
    }

    public Director getDirectorById(int id) {
        log.info("searching director by id " + id);
        return directorDao.findById(id);
    }

    @Override
    public Movie addMovie(Movie m) {
        log.info("about to add movie " + m);
        return movieDao.add(m);
    }

    @Override
    public Director addDirector(Director d) {
        log.info("about to add director " + d);
        return directorDao.add(d);
    }

}
