package vod.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vod.repository.CinemaDao;
import vod.repository.MovieDao;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;

import java.util.Set;
import java.util.logging.Logger;

@Component
public class CinemaServiceBean implements CinemaService {

    private static final Logger log = Logger.getLogger(CinemaService.class.getName());

    private CinemaDao cinemaDao;
    private MovieDao movieDao;

    @Autowired
    public CinemaServiceBean(CinemaDao cinemaDao, MovieDao movieDao) {
        log.info("creating cinema service bean");
        this.cinemaDao = cinemaDao;
        this.movieDao = movieDao;
    }

    public CinemaServiceBean(){
    }

    @Override
    public Cinema getCinemaById(int id) {
        log.info("searching cinema by id " + id);
        return cinemaDao.findById(id);
    }

    @Override
    public Set<Movie> getMoviesInCinema(Cinema c) {
        log.info("searching movies played in cinema " + c.getId());
        return movieDao.findByCinema(c);
    }

    @Override
    public Set<Cinema> getAllCinemas() {
        log.info("searching all cinemas");
        return cinemaDao.findAll();
    }

    @Override
    public Set<Cinema> getCinemasByMovie(Movie m) {
        log.info("searching cinemas by movie " + m.getId());
        return cinemaDao.findByMovie(m);
    }

    //@Autowired
    public void setCinemaDao(CinemaDao cinemaDao) {
        this.cinemaDao = cinemaDao;
    }

    //@Autowired
    public void setMovieDao(MovieDao movieDao) {
        this.movieDao = movieDao;
    }
}
