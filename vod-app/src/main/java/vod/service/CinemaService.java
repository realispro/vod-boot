package vod.service;

import vod.model.Cinema;
import vod.model.Movie;

import java.util.Set;

public interface CinemaService {

    Cinema getCinemaById(int id);

    Set<Cinema> getAllCinemas();

    Set<Cinema> getCinemasByMovie(Movie m);

    Set<Movie> getMoviesInCinema(Cinema c);

}
