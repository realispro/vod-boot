<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>VOD</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<c:url value = "/css/style.css"/>"/>
    <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet"/>

</head>

<body>
<header>
    <a href="./"><h1>Pix</h1></a>
    <h2>${title}</h2>
    <h3>
    </h3>
</header>
<section>
