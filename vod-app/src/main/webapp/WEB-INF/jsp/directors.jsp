<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<jsp:include page="./header.jsp" flush="true"/>

<table>
    <tr>
        <th>First name</th>
        <th>Last name</th>
    </tr>
    <c:forEach var="d" items="${directors}">
        <tr>
            <td>
                    ${d.firstName}
            </td>
            <td>
                <a href="./movies?directorId=${d.id}">${d.lastName}</a>
            </td>
        </tr>
    </c:forEach>

</table>

<jsp:include page="./footer.jsp" flush="true"/>
